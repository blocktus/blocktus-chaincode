# blocktus-chaincode
## BlueMix
This doesn't work - I suspect the repo must be public
[![Deploy to Bluemix](https://bluemix.net/deploy/button.png)](https://bluemix.net/deploy?repository=https://bitbucket.org:blocktus/blocktus-chaincode.git)

https://bluemix.net/deploy?repository=https://bitbucket.org:blocktus/blocktus-chaincode.git

## Clone & dev
Clone under fabic so it's synchronised by vagrant
```
cd $GOPATH/src/github.com/hyperledger/fabric
git clone git@bitbucket.org:blocktus/blocktus-chaincode.git
```
Intelij 16 + go plugin.
Create new project from existing source, then set the dir to $GOPATH/src/github.com/hyperledger/fabric/blocktus-chaincode

## Vagrant
```
alias cddevenv='cd ~/devroot/gocode/src/github.com/hyperledger/fabric/devenv'
cddevenv
vagrant ssh
```

If vagrant has a problem
```
vagrant provision
```

#### Teminal 1 - the validating peer
```
vagrant ssh
cd $GOPATH/src/github.com/hyperledger/fabric
make peer
cd ./peer
./peer node start --peer-chaincodedev
```

#### Teminal 2 - the chaincode
```
vagrant ssh
cd $GOPATH/src/github.com/hyperledger/fabric/blocktus-chaincode/identity_code
go build
CORE_CHAINCODE_ID_NAME=mycc CORE_PEER_ADDRESS=0.0.0.0:30303 ./identity_code
```

#### Teminal 3 - the cli
```
vagrant ssh
```
Temp - use the cli. Later use the REST api

```
cd $GOPATH/src/github.com/hyperledger/fabric/peer
./peer chaincode deploy -n mycc -c '{"Function":"init", "Args": []}'
```

# REST api
Use Chrome plugin "Advanced REST Client"

## create user
http://localhost:3000/chaincode
```
{
  "jsonrpc": "2.0",
  "method": "invoke",
  "params": {
      "type": 1,
      "chaincodeID":{
          "name":"mycc"
      },
      "ctorMsg": {
         "function":"create_user",
         "args":["martin", "Belmont, CA 94002, USA", "1/1/1970"]
      }
  },
  "id": 3
}
```
### read user
http://localhost:3000/chaincode
```
{
  "jsonrpc": "2.0",
  "method": "query",
  "params": {
      "type": 1,
      "chaincodeID":{
          "name":"mycc"
      },
      "ctorMsg": {
         "function":"read",
         "args":["martin"]
      }
  },
  "id": 5
}
```
Example response
```
{"name":"sydney","address":"Belmont, CA 94002, USA","dateofbirth":"1/1/1970","verifications":[{"verificationdate":"2/2/1970","what":"Passport","with":"GreenId","by":"martin","score":20,"scoretype":"SCORE_100"},{"verificationdate":"2/2/1970","what":"Drivers License","with":"GreenId","by":"martin","score":20,"scoretype":"SCORE_100"},{"verificationdate":"2/2/1970","what":"Gas Bill","with":"GreenId","by":"martin","score":5,"scoretype":"SCORE_100"},{"verificationdate":"Sun, 15 May 2016 11:07:19 UTC","what":"School Id","with":"GreenId","by":"martin","score":5,"scoretype":"SCORE_100"}]}
```

## add verification

http://localhost:3000/chaincode
```
{
  "jsonrpc": "2.0",
  "method": "invoke",
  "params": {
      "type": 1,
      "chaincodeID":{
          "name":"mycc"
      },
      "ctorMsg": {
         "function":"add_verification",
         "args":["sydney", "Passport", "GreenId", "martin", "20", "SCORE_100", ""]
      }
  },
  "id": 3
}
```

parameters are
* name - to whom the verification applies
* what - the physical thing being verified
* by - what is doing the verification. GreenID, FSV etc
* who - who is doing the verify. This is a person
* score - what's the score for this verify
* scoreType - the verification types (see below)
* blob - any type of data - base64 encode photo for example

Verification types
```
"SCORE_100"
"SCORE_CREDIT"
"SCORE_VFS"
```

Special notes on the response. The data is actually a string NOT a json object.

```
JSONObject result = d.getJSONObject("result");
String messageJson = result.getString("message");

JSONObject message = new JSONObject(messageJson);
JSONArray array = message.getJSONArray("verifications");
```

Example response
```
{
"jsonrpc": "2.0"
"result": {
"status": "OK"
"message": "{"name":"sydney","address":"Belmont, CA 94002, USA","dateofbirth":"1/1/1970","verifications":[{"verificationdate":"2/2/1970","what":"Passport","with":"GreenId","by":"martin","score":20,"scoretype":"SCORE_100","blob":""},{"verificationdate":"2/2/1970","what":"Drivers License","with":"GreenId","by":"martin","score":20,"scoretype":"SCORE_100","blob":""},{"verificationdate":"2/2/1970","what":"Gas Bill","with":"GreenId","by":"martin","score":5,"scoretype":"SCORE_100","blob":""},{"verificationdate":"Sun, 15 May 2016 11:07:19 UTC","what":"School Id","with":"GreenId","by":"martin","score":5,"scoretype":"SCORE_100","blob":""},{"verificationdate":"Fri, 20 May 2016 11:10:31 UTC","what":"Passport","with":"GreenId","by":"martin","score":20,"scoretype":"SCORE_100","blob":""},{"verificationdate":"Mon, 23 May 2016 04:14:01 UTC","what":"GreenId","with":"License","by":"user app","score":10,"scoretype":"SCORE_100","blob":"/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDABALDA4MChAODQ4SERATGCgaGBYWGDEjJR0oOjM9PDkzODdASFxOQERXRTc4UG1RV19iZ2hnPk1xeXBkeFxlZ2P/2wBDARESEhgVGC8aGi9jQjhCY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2NjY2P/wAARCAFQAL4DASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwACZqVUxTgMUtQajQKWiigQUUtFMBKKKWgBKKWigAooooAKKWigBKKWkoAKKKKACiiigAooooASilooGFFFFAgooooAKKWigBKKWkoAKKKKAFooooAKKKKACiiigBKKKKACiiigAooooAKSlpKAFopRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAJRRRQAUUUUAFFFFABSUtFAAKWmilFAC0UUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFJQAUUUUAFFFFIAooooAiDU8NUYpazuxkgalDVEDTgaOZgSZFLxUYNOzRzMB1GKQGjNPnYWFxRiiinzhYSiloo5kFhKKWinzIBKKXFJinzIApKWii4hKKWkpgFFFFIAooooAqhqcGqLNKDWRRMGpQaiBpQaQEoNOBqINTg1AEgNOqMNTgaAHUtIDRQAtFJRQAtFFFABRRRQAlFFFABRRRQAUlLRTuwEoooppu4ihmgGmg0tIY8GlBpgNKDSAfmlBpgNLQBIGp4NRCnCgCUGlBpgpRQA/NLmmiloAXNLmm0tAC0ZpKKAFopKKAFopKKAFooooAKSlpKcdwM2lptKKQC0opKUUAOFKKbThQA4U4U0U4UAPFOFMFOFADxThTRSigBaWiigBMUtN8xegyT7Ubj/cb9KAFoo3DvkfWloAKKKKAAUUUUAFFFFNbiMugUlKKBiiloFLSAUUopopwoAcKcKaKcKAHCnimCnigBwpwpgpu4uxVOAOrf4UASGTnCjJ/lSbC33zn27UqKFGBTqAADFLRRQAdaafk5/h/lTqTqOaAFopkZ4KnqpxT6ACiiigAoopRQBkilFNFOFMBaUUgpRSAWnCminCmA4U4U0UopAPFOFNFI7EABfvHgUAKSXOxTj+8alVQowKbGoRcD8/WlZtvA5Y9BQIcWC/U9qMM3U4HoKRExyTlvWpKBiKoUYFMlJGOcDPJFSUYoAZExaME9adS0UAR9Jz/tL/L/APXUlMb/AFqfQ1JQAlFLRQAUCigUAY4p1NFKKYDhS00U4UgHCnCmCnCgBwpwpopwoAcKSP53L9hwKRm2oT6CpI12oB6CgRIKbD8wLnqT+lOFMAMZJAyp5IHUUDJhS01WVh8pzTqACoSWMxXnjGPTHepqKAClpKKAGnmVfoafTF5lb2AFPoAKKWigBKWiigDEBpQaiDU8NTESA04GowaUGgZIDTgajBpwNAEgNOFRg08GkASfc/EfzqdagcbkIHWpI23KD60ASinCmA08UAKUVuSoJ9aPLT0zSinCgAAAGBRRRQAh4pTgDJppO5wo7cmmv87bB0/iP9KAFiHy5PVjmn0vaigAoopaBCUUtFAzmw1PBquDUgaqJJw1OBqEGnKaBkwNOBqMGnA0hkgNPBqIGnikBIDSA+W2f4T+lANOFAEqmpAarqpH3Dj27U8M4/hB+hoAnBpwqEM/9wfnSjzD/dH60ATZ4pm4twn/AH12pBGD94lvrUg6UANWMAY5P9acABwBilooAKKKWgAooooAKKKKAOTDU4GoQ1PBqySdTTwagBqRTQBMpqRTUCmpFNIZMDThUamng0hkgp4qMU8UgJFNSCohTxQBIKcKYKcKAHA04GmClBoAfmlzTAaXNADqKTNLQAtFJS0AFFFFAHGA09TUYp4qyCVTT1NRLTxQBKDUimohT1pDJlNSKahU1KpoGSqaeKiWpBUjJBTxUYp60ASCnUwU6gB1ApKKAHA0uabmjNADwaXNMzSg0APzRmmg0uaAFzS5ptFAHHCnimCnirIHCnimCnigCRaetRinrQMlFPU1GtSLSGSqakWoRUqmkMkWpBUQqRaQEgp1MFOoAWiiigBaKSloAWgGkooAeDRmm0uaAHZozTc1E9xHGcM3NAHKinimCnA1pYgkFOFRg08GiwEgqQVCGqQNRZjuSipFNQq1SKaOVgTLT1qJTUimlysZMtPWo1NSKaORgPFPFNXFPGMjvRyMLiUtOAA6inAL6Z/rRyMLkdFSgLj7v41IETuv/wBajkYXK9FWQseMkLjoar3V9a24I2hn9AKORhcbmoZbuOLgnJ9BVKa5mum4AjX0UU+CzJ5x+Jo5AuI9xNMcD5F/WnxWrMM4J+tXbeyJ+4pbHU9hWgliFA8xvy6VSikBwYpRTM04GqIHg04GowacDQMkBp4NRA04GgCZTUqmq4NSK1AFlTUq1XQ1MhoGTLUq9KhU1KtAEqmnj0qJakB4oAeDS8fh6UzNDOEGScY70ASZGMHimTXUcCZdh9PWs+41HJKwDr3qskTzNucliaAJ59QnnysWUQ02C0ZznGfUmrUFoFwW5PpV+OIdG4x0AoArQ2yr91dx/SrsduufnOT/AHRThwMKBj0pwOeMfhQBdgb5SmAidxS5xGobBx2/rVVDtPHH1p+/gj3yBQM87zSg02igkeDSg0wGnA0CHg04GowacDQMkBqRTUINOU0AWUap0aqimpUagZcRqmU1URqmV6ALINOBqDzVRcscVTmvmfKQ9PWgC9PeJCMZyfSs95JblvmJC+lJDAztk5J9a0ILYL1GTQBBb2nqMCr8UQUfKMD1p6pgc07NAD1CqOOfWnZ7Y/Gox707nHtQA8HBpwOcc5qMEdOv4UA0AThsfxAkdyaeshycY/Kq24dqcHxzmgZwtFApcUEiUooooAUGnA0ylFADwaeDUQp4NAEympFbFVw1O3gDmgC0r0NdBBgcmqRmZuFp0aEn1NAEpaSY/MTj0q5bW2cZGBTbeIDk1ejwBQMliiCjiphgCoQ1PDZoAfmlpoNKKAHCl6c00UAmgBwoB5puaWgBwJz/AIUoOfrTM0ZoGccKXFIKcBQiRuKMU/FIRTAZSilxSdKQBnFG6m5ppagCQyY6Ugyx5pqrk1Yij7mgBYo81cjQKKjQADipAaAJ1bFSq1VlNSKaBlpWqQNVVTUqNQBZU0/PFQqaeDxQA+lzTc0uaBi5pc03NFADqKTNGaYH/9k="}]}"
}-
"id": 5
}
```

Notes on image compression - use 50%. Compromise between size & quality.

## delete all verifications
```
{
  "jsonrpc": "2.0",
  "method": "invoke",
  "params": {
      "type": 1,
      "chaincodeID":{
          "name":"mycc"
      },
      "ctorMsg": {
         "function":"delete_verifications",
         "args":["martin"]
      }
  },
  "id": 3
}
```

## add permisssion
```
{
  "jsonrpc": "2.0",
  "method": "invoke",
  "params": {
      "type": 1,
      "chaincodeID":{
          "name":"mycc"
      },
      "ctorMsg": {
         "function":"delete_permissions",
         "args":["martin"]
      }
  },
  "id": 3
}
```

## delete all permisssion
```
{
  "jsonrpc": "2.0",
  "method": "invoke",
  "params": {
      "type": 1,
      "chaincodeID":{
          "name":"mycc"
      },
      "ctorMsg": {
         "function":"delete_permissions",
         "args":["martin"]
      }
  },
  "id": 3
}
```
