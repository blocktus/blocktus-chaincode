/*
	@c Twenty71.com
*/

package main

import (
	"errors"
	"fmt"
	"strconv"
	"encoding/json"
	"github.com/hyperledger/fabric/core/chaincode/shim"
	"time"
)

// SimpleChaincode example simple Chaincode implementation
type SimpleChaincode struct {
}

//var marbleIndexStr = "_marbleindex"				//name for the key/value that will store a list of all known marbles
//var openTradesStr = "_opentrades"				//name for the key/value that will store all open trades
//
//type Marble struct{
//	Name string `json:"name"`					//the fieldtags are needed to keep case from bouncing around
//	Color string `json:"color"`
//	Size int `json:"size"`
//	User string `json:"user"`
//}

//==============================================================================================================================
//	 Structure Definitions
//==============================================================================================================================
//	Chaincode - A blank struct for use with Shim (A HyperLedger included go file used for get/put state
//				and other HyperLedger functions)
//==============================================================================================================================

type Person struct {
	Name string `json:"name"`
	Address string `json:"address"`
	DateOfBirth string `json:"dateofbirth"`
	Verifications[] Verification `json:"verifications"`
	Permissions[] Permission `json:"permissions"`
}

type Verification struct {
	VerificationDate string `json:"verificationdate"`
	What string 	`json:"what"`
	With string 	`json:"with"`
	By string 		`json:"by"`
	Score int 		`json:"score"`
	ScoreType string `json:"scoretype"`
	Blob string		`json:"blob"`
}

type Permission struct {
	PermissionDate 	string `json:"permissiondate"`
	What 		string `json:"what"`
}

const   SCORE_100 	=  "SCORE_100"
const   SCORE_CREDIT    =  "SCORE_CREDIT"
const   SCORE_VFS       =  "SCORE_VFS"

const PARTY_WESTPAC = "WESTPAC"
const PARTY_AUSGRID = "AUSGRID"
const PARTY_QANTAS  = "QANTAS"
const PARTY_GREENID = "GREENID"


const DATE_TIME_FORMAT  = time.RFC1123

// ============================================================================================================================
// Main
// ============================================================================================================================
func main() {
	err := shim.Start(new(SimpleChaincode))
	if err != nil {
		fmt.Printf("Error starting Simple chaincode: %s", err)
	}
}

// ============================================================================================================================
// Init - reset all the things
// ============================================================================================================================
func (t *SimpleChaincode) Init(stub *shim.ChaincodeStub, function string, args []string) ([]byte, error) {
	return nil, nil
}

// ============================================================================================================================
// Run - Our entry point for Invocations - [LEGACY] obc-peer 4/25/2016
// ============================================================================================================================
func (t *SimpleChaincode) Run(stub *shim.ChaincodeStub, function string, args []string) ([]byte, error) {
	fmt.Println("run is running " + function)
	return t.Invoke(stub, function, args)
}

// ============================================================================================================================
// Invoke - Our entry point for Invocations
// ============================================================================================================================
func (t *SimpleChaincode) Invoke(stub *shim.ChaincodeStub, function string, args []string) ([]byte, error) {
	fmt.Println("invoke is running " + function)

	// Handle different functions
	if function == "init" {													//initialize the chaincode state, used as reset
		return t.Init(stub, "init", args)
	} else if function == "create_user" {
		return t.create_user(stub, args)
	} else if function == "add_verification" {
		return t.add_verification(stub, args)
	} else if function == "delete_verifications" {
		return t.delete_verifications(stub, args)
	} else if function == "add_permission" {
		return t.add_permission(stub, args)
	} else if function == "delete_permissions" {
		return t.delete_permissions(stub, args)
	} else if function == "verifications" {
		return t.delete_verifications(stub, args)
	} else if function == "delete" {
		return t.Delete(stub, args)											//deletes an entity from its state
	}
//	} else if function == "write" {											//writes a value to the chaincode state
//		return t.Write(stub, args)
//	} else if function == "init_marble" {									//create a new marble
//		return t.init_marble(stub, args)
//	} else if function == "set_user" {										//change owner of a marble
//		return t.set_user(stub, args)
//	}
	fmt.Println("invoke did not find func: " + function)					//error

	return nil, errors.New("Received unknown function invocation")
}

// ============================================================================================================================
// Query - Our entry point for Queries
// ============================================================================================================================
func (t *SimpleChaincode) Query(stub *shim.ChaincodeStub, function string, args []string) ([]byte, error) {
	fmt.Println("query is running " + function)

	// Handle different functions
	if function == "read" {													//read a variable
		return t.read(stub, args)
	}
	fmt.Println("query did not find func: " + function)						//error

	return nil, errors.New("Received unknown function query")
}

// ============================================================================================================================
// Read - read a variable from chaincode state
// ============================================================================================================================
func (t *SimpleChaincode) read(stub *shim.ChaincodeStub, args []string) ([]byte, error) {
	var name, jsonResp string
	var err error

	if len(args) != 1 {
		return nil, errors.New("Incorrect number of arguments. Expecting name of the var to query")
	}

	name = args[0]
	valAsbytes, err := stub.GetState(name)									//get the var from chaincode state
	if err != nil {
		jsonResp = "{\"Error\":\"Failed to get state for " + name + "\"}"
		return nil, errors.New(jsonResp)
	}

	return valAsbytes, nil													//send it onward
}

// ============================================================================================================================
// Delete - remove a key/value pair from state
// ============================================================================================================================
func (t *SimpleChaincode) Delete(stub *shim.ChaincodeStub, args []string) ([]byte, error) {
	if len(args) != 1 {
		return nil, errors.New("Incorrect number of arguments. Expecting 1")
	}

	name := args[0]
	err := stub.DelState(name)													//remove the key from chaincode state
	if err != nil {
		return nil, errors.New("Failed to delete state")
	}

	//get the marble index
	//marblesAsBytes, err := stub.GetState(marbleIndexStr)
	//if err != nil {
	//	return nil, errors.New("Failed to get marble index")
	//}
	//var marbleIndex []string
	//json.Unmarshal(marblesAsBytes, &marbleIndex)								//un stringify it aka JSON.parse()
    //
	////remove marble from index
	//for i,val := range marbleIndex{
	//	fmt.Println(strconv.Itoa(i) + " - looking at " + val + " for " + name)
	//	if val == name{															//find the correct marble
	//		fmt.Println("found marble")
	//		marbleIndex = append(marbleIndex[:i], marbleIndex[i+1:]...)			//remove it
	//		for x:= range marbleIndex{											//debug prints...
	//			fmt.Println(string(x) + " - " + marbleIndex[x])
	//		}
	//		break
	//	}
	//}
	//jsonAsBytes, _ := json.Marshal(marbleIndex)									//save new index
	//err = stub.PutState(marbleIndexStr, jsonAsBytes)
	return nil, nil
}

// ============================================================================================================================
// Write - write variable into chaincode state
// ============================================================================================================================
func (t *SimpleChaincode) Write(stub *shim.ChaincodeStub, args []string) ([]byte, error) {
	var name, value string // Entities
	var err error
	fmt.Println("running write()")

	if len(args) != 2 {
		return nil, errors.New("Incorrect number of arguments. Expecting 2. name of the variable and value to set")
	}

	name = args[0]															//rename for funsies
	value = args[1]
	err = stub.PutState(name, []byte(value))								//write the variable into the chaincode state
	if err != nil {
		return nil, err
	}
	return nil, nil
}

// ============================================================================================================================
// Creates a new user
//
// "function":"add_verification",
// "args":["martin", "Belmont, CA 94002, USA", "1/1/1970"]
//
// ============================================================================================================================
func (t *SimpleChaincode) create_user(stub *shim.ChaincodeStub, args []string) ([]byte, error) {
	var err error

	//   0       1       2     3
	// name address dob
	// martin belmont 1970
	if len(args) != 3 {
		return nil, errors.New("Incorrect number of arguments. Expecting 3")
	}

	fmt.Println("- start create_user")

	var person Person
	person.Name = args[0]
	person.Address = args[1]
	person.DateOfBirth = args[2]

	//verify
	if len(person.Name) <= 0 {
		return nil, errors.New("Name argument must be a non-empty string")
	}
	if len(person.Address) <= 0 {
		return nil, errors.New("Address argument must be a non-empty string")
	}
	if len(person.DateOfBirth) <= 0 {
		return nil, errors.New("DateOfBirth argument must be a non-empty string")
	}

	//unique name check
	record, err := stub.GetState(person.Name)
	if record != nil {
		var jsonResp = "{\"Error\":\"Failed person " + person.Name + " exists\"}"
		return nil, errors.New(jsonResp)
	}


	personAsBytes, _ := json.Marshal(person)
	err = stub.PutState(person.Name, personAsBytes)

	if err != nil {
		return nil, err
	}

	fmt.Println("- end create_user")
	return nil, nil
}

// ============================================================================================================================
// Creates a new verification
//      "function":"add_verification",
//		"args":["sydney", "Passport", "GreenId", "martin", "20", "SCORE_100"]
// ============================================================================================================================
func (t *SimpleChaincode) add_verification(stub *shim.ChaincodeStub, args []string) ([]byte, error) {
	var err error
	var verification Verification
	var name string
	var scoreType string
	var jsonResp string
	//var bytesResp []byte

	//   0       1       2     3		4		5         6
	// who 		what	with	by		score	scoreType blob
	// martin  passport	greenid	user	10		100point

	fmt.Println("- start add_verification")

	if len(args) != 7 {
		//jsonResp = "{\"Error\":\"Failed to get state for xxx\"}"
		//bytesResp, err :=json.Marshal(jsonResp)
		return nil, errors.New("Incorrect number of arguments. Expecting 7")
	}

	name = args[0]
	//check score is a number
	score, err := strconv.Atoi(args[4])
	if err != nil { return nil, errors.New("Invalid Score. Should be number")}

	//check scoreType is a valid value
	scoreType = args[5]
	if (! (scoreType == SCORE_100 || scoreType == SCORE_CREDIT || scoreType == SCORE_VFS)) {
		return nil, errors.New("Invalid ScoreType. Should be SCORE_100, SCORE_CREDIT, or SCORE_VFS")
	}

	verification.VerificationDate = time.Now().Format(DATE_TIME_FORMAT)
	verification.What = args[1]
	verification.With = args[2]
	verification.By = args[3]
	verification.Score = score
	verification.ScoreType = scoreType
	verification.Blob = args[6]


	//check for non empty
	if len(name) <= 0 {
		return nil, errors.New("Name must be a non-empty string")
	}
	if len(verification.What) <= 0 {
		return nil, errors.New("What must be a non-empty string")
	}
	if len(verification.With) <= 0 {
		return nil, errors.New("With must be a non-empty string")
	}
	if len(verification.By) <= 0 {
		return nil, errors.New("By must be a non-empty string")
	}

	fmt.Println("- add_verification for" + name + " what=" + verification.What);

	//load the user
	userAsbytes, err := stub.GetState(name)									//get the var from chaincode state
	if err != nil {
		fmt.Println("- add_verification cant find " + name)
		jsonResp = "{\"Error\":\"Failed to get state for " + name + "\"}"
		return nil, errors.New(jsonResp)
	}

	var person Person
	err = json.Unmarshal(userAsbytes, &person)
	if err != nil {
		fmt.Println("- add_verification cant read data for " + name)
		return nil, errors.New("Failed to Unmarshal person" + name)
	}

	person.Verifications = append(person.Verifications, verification)

	//Commit updates batch to ledger
	fmt.Println("- add_verification Commit Updates To Ledger");
	personAsBytes, _ := json.Marshal(person)
	err = stub.PutState(name, personAsBytes)
	if err != nil {
		fmt.Println("- add_verification some error");
		return nil, err
	}

	fmt.Println("- end add_verification")
	return nil, nil
}

// ============================================================================================================================
// Creates a new verification
//      "function":"delete_verifications",
//		"args":["sydney"]
// ============================================================================================================================
func (t *SimpleChaincode) delete_verifications(stub *shim.ChaincodeStub, args []string) ([]byte, error) {
	var err error
	var verification Verification
	var name string
	var jsonResp string
	//var bytesResp []byte

	//   0
	// who
	// martin

	fmt.Println("- start delete_verifications")

	if len(args) != 1 {
		//jsonResp = "{\"Error\":\"Failed to get state for xxx\"}"
		//bytesResp, err :=json.Marshal(jsonResp)
		fmt.Println("- delete_verifications error args count not 1");
		return nil, errors.New("Incorrect number of arguments. Expecting 1")
	}

	name = args[0]

	//check for non empty
	if len(name) <= 0 {
		fmt.Println("- delete_verifications error name empty=");
		return nil, errors.New("Name must be a non-empty string")
	}

	fmt.Println("- delete_verifications for" + name + " what=" + verification.What);

	//load the user
	userAsbytes, err := stub.GetState(name)									//get the var from chaincode state
	if err != nil {
		fmt.Println("- delete_verifications cant find " + name)
		jsonResp = "{\"Error\":\"Failed to get state for " + name + "\"}"
		return nil, errors.New(jsonResp)
	}

	var person Person
	err = json.Unmarshal(userAsbytes, &person)
	if err != nil {
		fmt.Println("- delete_verifications cant read data for " + name)
		return nil, errors.New("Failed to Unmarshal person" + name)
	}

	person.Verifications = nil


	//Commit updates batch to ledger
	fmt.Println("- add_verification Commit Updates To Ledger");
	personAsBytes, _ := json.Marshal(person)
	err = stub.PutState(name, personAsBytes)
	if err != nil {
		fmt.Println("- delete_verifications some error");
		return nil, err
	}

	fmt.Println("- end delete_verifications")
	return nil, nil
}

// ============================================================================================================================
// Creates a new verification
//      "function":"add_permissions",
//		"args":["martin", "WESTPAC"]
// ============================================================================================================================
func (t *SimpleChaincode) add_permission(stub *shim.ChaincodeStub, args []string) ([]byte, error) {
	var err error
	var permission Permission
	var name string

	//var bytesResp []byte

	//   0       1
	// who 	   permission
	// martin  WESTPAC

	fmt.Println("- start add_permission")

	if len(args) != 2 {
		//jsonResp = "{\"Error\":\"Failed to get state for xxx\"}"
		//bytesResp, err :=json.Marshal(jsonResp)
		return nil, errors.New("Incorrect number of arguments. Expecting 2")
	}

	name = args[0]

	permission.PermissionDate = time.Now().Format(DATE_TIME_FORMAT)
	permission.What = args[1]

	//check for non empty
	if len(name) <= 0 {
		return nil, errors.New("Name must be a non-empty string")
	}
	if len(permission.What) <= 0 {
		return nil, errors.New("What must be a non-empty string")
	}

	fmt.Println("- add_permission for" + name + " what=" + permission.What);

	//load the user
	userAsbytes, err := stub.GetState(name)									//get the var from chaincode state
	if err != nil {
		fmt.Println("- add_permission cant find " + name)
		return nil, errors.New("{\"Error\":\"Failed to get state for " + name + "\"}")
	}

	var person Person
	err = json.Unmarshal(userAsbytes, &person)
	if err != nil {
		fmt.Println("- add_permission cant read data for " + name)
		return nil, errors.New("Failed to Unmarshal person" + name)
	}

	person.Permissions = append(person.Permissions, permission)

	//Commit updates batch to ledger
	fmt.Println("- add_permission Commit Updates To Ledger");
	personAsBytes, _ := json.Marshal(person)
	err = stub.PutState(name, personAsBytes)
	if err != nil {
		fmt.Println("- add_permission some error");
		return nil, err
	}

	fmt.Println("- end add_permission")
	return nil, nil
}

// ============================================================================================================================
// Creates a new verification
//      "function":"delete_verifications",
//		"args":["sydney"]
// ============================================================================================================================
func (t *SimpleChaincode) delete_permissions(stub *shim.ChaincodeStub, args []string) ([]byte, error) {
	var err error
	var verification Verification
	var name string
	var jsonResp string
	//var bytesResp []byte

	//   0
	// who
	// martin

	fmt.Println("- start delete_permissions")

	if len(args) != 1 {
		//jsonResp = "{\"Error\":\"Failed to get state for xxx\"}"
		//bytesResp, err :=json.Marshal(jsonResp)
		fmt.Println("- delete_permissions error args count not 1");
		return nil, errors.New("Incorrect number of arguments. Expecting 1")
	}

	name = args[0]

	//check for non empty
	if len(name) <= 0 {
		fmt.Println("- delete_permissions error name empty=");
		return nil, errors.New("Name must be a non-empty string")
	}

	fmt.Println("- delete_verifications for" + name + " what=" + verification.What);

	//load the user
	userAsbytes, err := stub.GetState(name)                                                                        //get the var from chaincode state
	if err != nil {
		fmt.Println("- delete_permissions cant find " + name)
		jsonResp = "{\"Error\":\"Failed to get state for " + name + "\"}"
		return nil, errors.New(jsonResp)
	}

	var person Person
	err = json.Unmarshal(userAsbytes, &person)
	if err != nil {
		fmt.Println("- delete_permissions cant read data for " + name)
		return nil, errors.New("Failed to Unmarshal person" + name)
	}

	person.Permissions = nil


	//Commit updates batch to ledger
	fmt.Println("- delete_permissions Commit Updates To Ledger");
	personAsBytes, _ := json.Marshal(person)
	err = stub.PutState(name, personAsBytes)
	if err != nil {
		fmt.Println("- delete_permissions some error");
		return nil, err
	}

	fmt.Println("- end delete_permissions")
	return nil, nil
}